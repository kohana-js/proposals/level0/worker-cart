/* istanbul ignore file */
addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
});

const {Router} = require('cf-worker-router');
const WorkerController = require('./classes/WorkerController');
const ControllerHome = require('./classes/controller/Home');
const ControllerCart = require('./classes/controller/Cart');
const ControllerFetch = require('./classes/controller/Fetch');

async function handleRequest(request) {
    const r = new Router();
    r.get('/user/:id/:action', async request => new Response(`user ${request.params.id}, ${request.params.action}`));

    r.get('/', WorkerController.handlerFactory(ControllerHome));

    r.get('/fetch', WorkerController.handlerFactory(ControllerFetch, {action: 'fetch'}));

    r.get('/add', WorkerController.handlerFactory(ControllerCart, {action: 'view_add'}));

    r.post('/add', WorkerController.handlerFactory(ControllerCart, {action: 'post_add'}));

    r.post('/:domain/add', WorkerController.handlerFactory(ControllerCart, {action: 'post_add'}));

    r.get('/clear', WorkerController.handlerFactory(ControllerCart, {action: 'clear'}));

    return await r.lookup(request);
}

module.exports = {
  handleRequest : handleRequest,
};