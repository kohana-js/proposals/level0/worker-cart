const {Controller} = require('@kohanajs/core-mvc');
const $ = ref => (typeof ref === 'function')? ref() : ref;
const qs = require('qs');
const eq = require('fast-deep-equal');

const ControllerMixinCookie = require('../controllerMixin/Cookie');
const ControllerMixinCloudFlareKV = require('../controllerMixin/CloudFlareKV');

class ControllerCart extends Controller{
  constructor(request) {
    super(request);
    this.addMixin(new ControllerMixinCookie(this, 'TnLSCzt8Kd5i5PGz0FjudvapySVyxN7YQ/3FFPBXsE5AX5nARMAzHO1eL2kxpwx4QKgxsHnY80xl9O5NUatsag==', {payloadMaxSize: 65536}));
    this.addMixin(new ControllerMixinCloudFlareKV(this));
  }

  async action_post_add() {
    const referrer = new URL(this.request.referrer || this.request.headers.get('referer') || 'https://www.shop-steam.com/add/');

    if(!!this.request.params.domain){
      if(this.request.params.domain !== referrer.hostname){
        return this.forbidden('Bot behaviour detected.');
      }
    }

    //parse request text as $_POST;
    const body = await this.request.text();
    const $_POST = (this.request.headers.get('content-type') === 'application/json') ? JSON.parse(body) : qs.parse(body);

    const items = $_POST['items'] || [{
      id : $_POST['id'],
      quantity : parseInt($_POST['quantity']),
      properties : $_POST['properties'],
    }];

    const payload = $(this.payload);

    const carts = payload.carts || {ct : Date.now(), ut: 0};
    carts[referrer.hostname] = carts[referrer.hostname] || {};
    const cart = carts[referrer.hostname];

    //modify cart
    items.forEach(x => {
      const id = x.id;
      const properties = x.properties || {};
      const quantity   = x.quantity || 0;
      //variant id cannot be reserved words

      cart[id] = cart[id] || [];
      const cartItems = cart[id];

      //cart item group by properties
      for(let i = 0; i < cartItems.length; i++){
        if( eq(cartItems[i][0], properties) ){
          cartItems[i][1] += quantity;
          return;
        }
      }

      cartItems.push([properties, quantity]);
    });

    cart.ut = Date.now();

    await this.addSecureCookie('payload', JSON.stringify(Object.assign(payload, {carts: carts})));

    this.headers['Content-type'] = 'application/json; charset=utf-8';
    this.body = payload;
  }

  async action_view_add() {
    this.body = require('../../template/add.html');
  }

  async action_clear() {
    this.headers['Content-type'] = 'application/json; charset=utf-8';
    this.body = {type: 'CLEAR', payload: {success: true}};
  }
}

module.exports = ControllerCart;