const {Controller} = require('@kohanajs/core-mvc');

class ControllerHome extends Controller{
  async action_index (){
    this.headers['Content-type'] = 'application/json; charset=utf-8';
    this.body = JSON.stringify({hello: 'world!!'});
  }
}

module.exports = ControllerHome;