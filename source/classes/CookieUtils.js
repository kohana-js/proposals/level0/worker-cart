const qs = require('qs');
const {encode, decode} = require('base64-arraybuffer');

const sign = async (val, key) => {
  const buffer = new TextEncoder().encode(val);
  const sign = await crypto.subtle.sign("HMAC", key, buffer);
  return val + '.' + encode(sign).replace(/\=+$/, '');
};

const unsign = async (val, key = null) => {
  if(!val)return null;

  const unsignValue = val.replace(/\.[^.]+$/, '');
  if(!key)return unsignValue;

  if(val !== await sign(unsignValue, key))return false;
  return unsignValue;
};

const parseCookie = str => {
  const rawCookie = str || '';
  const reqCookie = {};
  rawCookie.split(/; */g).forEach(x => Object.assign(reqCookie, qs.parse(x)));
  return reqCookie
};

module.exports = {
  parseCookie : parseCookie,
  sign: sign,
  unsign : unsign
};