const InventoryFile = require('../source/classes/InventoryFile');

describe('Inventory file test', ()=>{
  test('header', ()=>{
      const f = new InventoryFile(1584471788, 16);
      expect(f.toString()).toBe('01011110 01110001 00011110 11101100 00010000 : 0000000000000000 0000000000000000 ');
      expect(f.header.getUint32(0)).toBe(1584471788);
      expect(f.header.getUint8(4)).toBe(16);
  });

  test('init with qty', () => {
    const f = new InventoryFile(1584471788, 16, [12000, 2413]);

    expect(f.toString()).toBe('01011110 01110001 00011110 11101100 00010000 : 0010111011100000 0000100101101101 ');

    expect(f.read(0)).toBe(12000n);
    expect(f.read(1)).toBe(2413n);
  });

  test('init with qty, 10 bit size', () => {
    const f = new InventoryFile(1584471788, 10, [520, 1023]);

    expect(f.toString()).toBe('01011110 01110001 00011110 11101100 00001010 : 1000001000 1111111111 0000');

    expect(f.read(0)).toBe(520n);
    expect(f.read(1)).toBe(1023n);

    f.write(3, 888);
    expect(f.read(3)).toBe(888n);
    expect(f.toString()).toBe('01011110 01110001 00011110 11101100 00001010 : 1000001000 1111111111 0000000000 1101111000 00000000');

    f.write(2, 56);
    expect(f.read(3)).toBe(888n);
    expect(f.read(2)).toBe(56n);
    expect(f.toString()).toBe('01011110 01110001 00011110 11101100 00001010 : 1000001000 1111111111 0000111000 1101111000 00000000');
  });

  test('encode decode', ()=>{
    const f = new InventoryFile(1584471788, 8, [120, 200, 80, 55]);
    expect(f.toString()).toBe('01011110 01110001 00011110 11101100 00001000 : 01111000 11001000 01010000 00110111 ');
    expect(f.encode()).toBe('XnEe7Ah4yFA3');

    const f2 = InventoryFile.decode('XnEe7Ah4yFA3');
    expect(f2.read(0)).toBe(120n);
    expect(f2.read(1)).toBe(200n);
    expect(f2.read(2)).toBe(80n);
    expect(f2.read(3)).toBe(55n);

    try{
      f2.read(4);
    }catch(e){
      expect(e.message).toBe('Offset is outside the bounds of the DataView');
    }
  });

  test('expand file', ()=>{
    const f = new InventoryFile(1584471788, 8, [120, 200, 80, 55]);
    expect(f.toString()).toBe('01011110 01110001 00011110 11101100 00001000 : 01111000 11001000 01010000 00110111 ');
    f.write(4, 33);
    expect(f.toString()).toBe('01011110 01110001 00011110 11101100 00001000 : 01111000 11001000 01010000 00110111 00100001 00000000 00000000 00000000 ');
  });

  test('default values', ()=>{
    const f = new InventoryFile();
    expect(f.toString()).toBe('00000000 00000000 00000000 00000001 00010000 : 0000000000000000 0000000000000000 ');
  })

});