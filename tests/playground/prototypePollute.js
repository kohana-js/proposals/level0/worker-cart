Object.freeze(Object.prototype);
Object.freeze(Object);

class A {
  constructor() {
    this.name = 'a';
  }
}

class B extends A{
  constructor() {
    super();
    this.name = 'b';
  }
}

A.prototype.toString = 'evil';

const a = new A();
const b = new B();

console.log(a.name, b.name);

console.log(a.toString);

a.__proto__.toString = 'more evil';
console.log(a.toString);

const {Controller} = require('@komino/k8-core-mvc');
Controller.prototype.execute = 'evil';

const c = new Controller({});
console.log(c.execute);

c.__proto__.execute = 'more evil';
console.log(c.execute);

const ControllerHome = require('../../source/classes/controller/Home');
const h = new ControllerHome({});
ControllerHome.prototype.action_index = 'evil';

console.log(c.execute);
h.__proto__.action_index = 'more evil';
console.log(c.execute);

ControllerHome.test = 'evil';
console.log(ControllerHome.test);

Controller.test = 'evil too';
console.log(Controller.test);